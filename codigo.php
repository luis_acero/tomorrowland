<?php
$esmayor = isset($_REQUEST['esmayor'])?$_REQUEST['esmayor']:0;
if(!$esmayor) {
    header('location: index.html');
    exit;
}
?>
<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="assets/favicon.png" type="image/x-icon">


    <!--Bootstrap-->
    <link rel="stylesheet" media="all" type="text/css" href="dist/lib.css">
    <script src="dist/lib.js"></script>
    <!--Modernizr-->
    <script src="dist/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <script type="text/javascript" src="js/script.js"></script>
    <link rel="stylesheet" media="all" type="text/css" href="css/styles.min.css">
    <!-- Ajax Loading effect -->
	<link href="css/loading.min.css" rel="stylesheet" type="text/css">	
    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    
        ga('create', 'UA-82822318-4', 'auto');
        ga('send', 'pageview');

    </script>
</head>

<body>
    <header class="container-fluid inside">
        <div class="container">
            <div class="col-xs-12 col-sm-2">
                <img src="assets/coljuegos.png" alt="" class="img-responsive">
            </div>
            <div class="col-xs-12 col-sm-offset-6 col-sm-4 terms-inside">
                <!--a class="terms" data-toggle="modal" data-target="#modal-terminos">Términos y condiciones</a-->
                <a class="terms" href="assets/terminos-y-condiciones-tomorrowland-con-bud.pdf" target="_blank">Términos y condiciones</a>
                <a class="terms" href="assets/puntos-venta-tml.png" target="_blank">Dónde Puedes Encontrar los Códigos</a>
            </div>
        </div>
    </header>
    <div class="container-fluid inside" id="main">
        <div class="container bud-concert">
            <div class="col-xs-12 col-sm-6 col-sm-offset-3 col-md-offset-3 col-md-5 text-center content">
                <h1>INGRESA TUS DATOS</h1>
                <!-- Alert de validacion de campos -->
                <div class="alert alert-danger hidden" id="alerta-campos">
                  <strong>Error!</strong> Debes rellenar todos los campos del formulario.
                </div>
                <!--/ Alert de validacion de campos -->
                <form class="form-horizontal" data-parsley-validate id="frm-codigo" method="post">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label class="control-label col-xs-4" for="nombre">NOMBRE</label>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control" id="nombre" required = "required">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label class="control-label col-xs-4" for="apellido">APELLIDO</label>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control" id="apellido" required = "required">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label col-xs-4 col-sm-2" for="documento">DOCUMENTO</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="text" class="form-control" id="documento" required= "required">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group">
                                <label class="control-label col-xs-4 col-sm-2" for="email">EMAIL</label>
                                <div class="col-xs-8 col-sm-10">
                                    <input type="email" class="form-control" id="email" required= "required">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label class="control-label col-xs-4" for="telefono">TELÉFONO</label>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control" id="telefono" required= "required">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="form-group">
                                <label class="control-label col-xs-4" for="ciudad">CIUDAD</label>
                                <div class="col-xs-8">
                                    <input type="text" class="form-control" id="ciudad" required= "required">
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="form-group direccion">
                                <label class="control-label col-xs-12 col-sm-2" for="direccion">DIRECCIÓN DE RESIDENCIA</label>
                                <div class="col-xs-12 col-sm-10">
                                    <div class="col-xs-12 col-sm-3">
                                        <div class="row">
                                            <select name="direccion" class="form-control" id="direccion">
                                            <option value="calle">CALLE</option>
                                            <option value="carrera">CARRERA</option>
                                            <option value="avenida">AVENIDA</option>
                                            <option value="transversal">TRANSVERSAL</option>
                                            <option value="diagonal">DIAGONAL</option>
                                        </select>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 col-sm-2">
                                        <div class="row">
                                            <input type="text" class="form-control" id="calle">
                                        </div>
                                    </div>
                                    <div class="col-xs-2 col-sm-1">
                                        <div class="row">
                                            <label class="control-label big" for="numero">#</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-2 col-sm-2">
                                        <div class="row">
                                            <input type="text" class="form-control" id="numero">
                                        </div>
                                    </div>
                                    <div class="col-xs-2 col-sm-1">
                                        <div class="row">
                                            <label class="control-label big" for="guion">-</label>
                                        </div>
                                    </div>
                                    <div class="col-xs-3 col-sm-3">
                                        <div class="row">
                                            <input type="text" class="form-control" id="guion">
                                        </div>
                                    </div>
                                    <div class="col-xs-12 adicional">
                                        <div class="row">
                                            <input type="text" class="form-control" id="adicional" placeholder="Ej Apartamente 207 interior 2">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="radio">
                                <p class="control-label col-xs-12 col-sm-5 col-sm-offset-2">¿CUENTAS CON PASAPORTE VIGENTE?</p>
                                <div class="col-xs-12 col-sm-5">
                                    <ul>

                                        <li> <label class="radio-inline">SI</label><input type="radio" name="optradio" id="s-option" required="" value="si">
                                            <div class="check"></div>
                                        </li>
                                        <li> <label class="radio-inline">NO</label><input type="radio" name="optradio" id="n-option" value="no">
                                            <div class="check"></div>
                                        </li>

                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 ">
                            <div class="form-group codigo">
                                <label for="" class="control-label col-xs-12 text-center">
                                    CÓDIGO
                                </label>
                                <div class="col-xs-12 col-sm-6 col-sm-offset-3">
                                    <input id="keycode" type="text" class="form-control" required= "required">
                                </div>
                            </div>
                        </div>
                        <div class="warning">
                            <p>
                                Ingresa el código usando minúsculas y <br>
                                mayúsculas tal como lo está en tu "raspe y gane”
                            </p>
                            <p>
                                *Verifica tus datos antes de enviar el formulario
                            </p>
                        </div>
                        <button type="submit" class="btn btn-send-01" id="btn-enviar">ENVIAR</button>
                    </div>
                    <input type="hidden" name="esmayor" id="es-mayor" value="<?php print $esmayor; ?>">
                </form>
            </div>
        </div>
    </div>
    <!--MODALES-->
    <div class="modal fade modal-type-01" id="modal-gorra" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <img src="assets/gorra.png" alt="" class="img-responsive">
                    <h4>CON EL CÓDIGO <span id ="codigo-gorra" class="codigo-ingr">xxxxxxxxx</span></h4>                    
                    <h3>¡GANASTE UNA GORRA!</h3>
                    <h5>TU PREMIO SERÁ ENVIADO A LA DIRECCIÓN QUE NOS INDICASTE</h5>
                    <p>El código con el que ganaste es <span id ="codigo-gorra2" class="codigo-ingr">xxxxxxxxx</span> ya te enviamos un correo con tus datos y algunas recomendaciones</p>
                </div>
                <div class="modal-footer">
                    <a href="#modal-datos" data-toggle="modal" data-dismiss="modal">ENVIAR PREMIO</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade modal-type-01" id="modal-datos" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1>VERIFICA TUS DATOS, NOSOTROS TE LLEVAMOS EL PREMIO</h1>
                </div>
                <div class="modal-body">                
                    <form id="frm-verifica" class="form-horizontal">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="nombre">NOMBRE</label>
                                    <div class="col-xs-8">
                                        <input type="text" class="form-control" id="ver-nombre">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="apellido">APELLIDO</label>
                                    <div class="col-xs-8">
                                        <input type="text" class="form-control" id="ver-apellido">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label col-xs-4 col-sm-2" for="documento">DOCUMENTO</label>
                                    <div class="col-xs-8 col-sm-10">
                                        <input type="text" class="form-control" id="ver-documento">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group">
                                    <label class="control-label col-xs-4 col-sm-2" for="email">EMAIL</label>
                                    <div class="col-xs-8 col-sm-10">
                                        <input type="email" class="form-control" id="ver-email">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="telefono">TELÉFONO</label>
                                    <div class="col-xs-8">
                                        <input type="text" class="form-control" id="ver-telefono">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label col-xs-4" for="ciudad">CIUDAD</label>
                                    <div class="col-xs-8">
                                        <input type="text" class="form-control" id="ver-ciudad">
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="form-group direccion">
                                    <label class="control-label col-xs-12 col-sm-2" for="direccion">DIRECCIÓN DE RESIDENCIA</label>
                                    <div class="col-xs-12 col-sm-10">
                                        <div class="col-xs-12 col-sm-3">
                                            <div class="row">
                                                <select name="direccion" class="form-control" id="ver-dir1">
                                            <option value="calle">CALLE</option>
                                            <option value="carrera">CARRERA</option>
                                            <option value="avenida">AVENIDA</option>
                                            <option value="transversal">TRANSVERSAL</option>
                                            <option value="diagonal">DIAGONAL</option>
                                        </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-3 col-sm-2">
                                            <div class="row">
                                                <input type="text" class="form-control" id="ver-dir2">
                                            </div>
                                        </div>
                                        <div class="col-xs-2 col-sm-1">
                                            <div class="row">
                                                <label class="control-label big" for="ver-dir3">#</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-2 col-sm-2">
                                            <div class="row">
                                                <input type="text" class="form-control" id="ver-dir3">
                                            </div>
                                        </div>
                                        <div class="col-xs-2 col-sm-1">
                                            <div class="row">
                                                <label class="control-label big" for="ver-dir4">-</label>
                                            </div>
                                        </div>
                                        <div class="col-xs-3 col-sm-3">
                                            <div class="row">
                                                <input type="text" class="form-control" id="ver-dir4">
                                            </div>
                                        </div>
                                        <div class="col-xs-12 adicional">
                                            <div class="row">
                                                <input type="text" class="form-control" id="ver-dir5" placeholder="Ej Apartamente 207 interior 2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="radio">
                                    <p class="control-label col-xs-12 col-sm-5 col-sm-offset-2">¿CUENTAS CON PASAPORTE VIGENTE?</p>
                                    <div class="col-xs-12 col-sm-5">
                                        <ul>

                                            <li> <label class="radio-inline">SI</label><input type="radio" name="ver-optradio" id="s-option-ver" value="si">
                                                <div class="check"></div>
                                            </li>
                                            <li> <label class="radio-inline">NO</label><input type="radio" name="ver-optradio" id="n-option-ver" value="no">
                                                <div class="check"></div>
                                            </li>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <!-- <button id="btn-verifica" type="submit" class="btn btn-send-01" href="#modal-gracias" data-toggle="modal" data-dismiss="modal">ENVIAR</button> -->
								<button id="btn-verifica" type="submit" class="btn btn-send-01">ENVIAR</button>
                            </div>
                        </div>
						<input id="id-usuario" name="id-usuario" type="hidden" value="">
                    </form>
                </div>
                <div class="modal-footer">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <div class="modal fade modal-type-01" id="modal-gracias" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="btn-correcto" type="button" class="close" data-dismiss="modal"><img src="assets/close-btn.png"></button>
                </div>
                <div class="modal-body">
                    <h1>TUS DATOS SE SUBIERON CORRECTAMENTE</h1>
                    <img src="assets/bud-ty.png" alt="" class="img-responsive">
                </div>
                <div class="modal-footer">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade modal-type-01" id="modal-nada" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="no-ganaste" type="button" class="close" data-dismiss="modal"><img src="assets/close-btn.png"></button>
                </div>
                <div class="modal-body">
                    <h1>¡NO GANASTE!</h1>
                    <h3>VUELVE A INTENTARLO COMPRANDO OTRA PROMO</h3>
                    <a href="assets/puntos-venta-tml.png" target="_blank">Entérate aquí de otra forma para ir al lugar más libre del mundo</a>
                    <img src="assets/cervezas-amigos.png" alt="" class="img-responsive">
                </div>
                <div class="modal-footer">
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->


    <div class="modal fade modal-type-01" id="modal-pack" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <img src="assets/pack.png" alt="" class="img-responsive">
                    <h4>CON EL CÓDIGO <span id="codigo-caja" class="codigo-ingr">xxxxxxxxx</span></h4>
                    <h3>¡GANASTE UNA CAJA DE 24 BUDS BOTELLA NO RETORNABLE DE 355ML PARA COMPARTIR!</h3>

                    <p>Te enviaremos un correo con tus datos, algunas recomendaciones y tu premio será enviado a la dirección que nos indicaste</p>
                </div>
                <div class="modal-footer">
                    <a href="#modal-datos" data-toggle="modal" data-dismiss="modal">ENVIAR PREMIO</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

    <div class="modal fade modal-type-01" id="modal-usado" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><img src="assets/close-btn.png"></button>
                </div>
                <div class="modal-body">
                    <img src="assets/usado.png" alt="" class="img-responsive">
                    <h3>ERROR: CÓDIGO YA USADO</h3>
                    <h4>SI NO TIENES TU CÓDIGO</h4>

                    <a href="assets/puntos-venta-tml.png" target="_blank">Entérate aquí de otra forma para ir al lugar más libre del mundo</a>
                </div>
                <div class="modal-footer">

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade modal-type-01" id="modal-error" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><img src="assets/close-btn.png"></button>
                </div>
                <div class="modal-body">
                    <img src="assets/error.png" alt="" class="img-responsive">
                    <h3>ERROR: CÓDIGO NO EXISTENTE</h3>
                    <h4>REVISA EL CÓDIGO QUE INGRESASTE Y VUELVE A ENVIAR EL FORMULARIO.</h4>

                    <h4>SI NO TIENES TU CÓDIGO</h4>

                    <a href="assets/puntos-venta-tml.png" target="_blank">Entérate aquí de otra forma para ir al lugar más libre del mundo</a>
                </div>
                <div class="modal-footer">

                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <div class="modal fade modal-type-01" id="modal-terminos" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><img src="assets/close-btn.png"></button>
                    </div>
                    <div class="modal-body">
                        <h3>ACTIVIDAD PROMOCIONAL – ENTREGA DE UN PAQUETE DOBLE PARA EL SEGUNDO FIN DE SEMANA DE TOMORROWLAND 2017
                        </h3>
                        <div class="terminos">
                            <h3>Cumplimiento ACTIVACIÓN COMERCIAL TOMORROWLAND 2017 al Consumidor</h3>
                            <ul>
                                <li>
                                    <p>

                                        <b>Identificación del producto:</b>
                                    </p>
                                    <ul>
                                        <li>
                                            <p>Cerveza Budweiser Botella NR de 343 ml</p>
                                        </li>
                                    </ul>
                                </li>

                                <li>
                                    <b>Identificación del incentivo que se ofrece indicando su cantidad y calidad:</b>

                                    <ul>
                                        <li>
                                            Un paquete doble para asistir el segundo fin de semana de Tomorrowland 2017 que incluye:
                                            <ul>
                                                <li>
                                                    Tiquetes aéreos ida y vuelta Bogotá - Bruselas en clase turista, saliendo el 26 de Julio de 2017 de Bogotá y regresando el 31 de Julio de 2017 de Bruselas.
                                                </li>
                                                <li>
                                                    Transporte terrestre desde el aeropuerto de Bruselas hasta el lugar del festival el 27 de Julio de 2017.
                                                </li>
                                                <li>
                                                    Transporte terrestre desde el festival hasta el aeropuerto de Bruselas el 31 de Julio de 2017.
                                                </li>
                                                <li>
                                                    Hospedaje del 27 de Julio de 2017 al 31 de Julio de 2017 en la zona “Brew District” en experiencia de camping 5 estrellas.
                                                </li>
                                                <li>
                                                    Bar abierto en la zona “Brew District” del 27 al 31 de Julio de 2017.
                                                </li>
                                                <li>
                                                    Alimentación tipo bufet (desayuno, almuerzo y cena) y bebidas en la zona Brew District del 27 al 31 de Julio de 2017.
                                                </li>
                                                <li>
                                                    Acceso a la piscina de la zona Brew District del 27 al 31 de Julio de 2017.
                                                </li>
                                                <li>
                                                    Acceso a la zona de spa de la zona Bew District del 27 al 31 de Julio de 2017.
                                                </li>
                                                <li>
                                                    Acceso a servicio de seguridad de la zona Brew District del 27 al 31 de Julio de 2017.
                                                </li>
                                                <li>
                                                    Acceso a servicio de conserje de la zona Brew District del 27 al 31 de Julio de 2017.
                                                </li>
                                                <li>
                                                    Entradas (manilla) en localidad VIP confirmar para los tres días del festival Tomorrowland 2017 en su segundo fin de semana – 28 al 30 de julio de 2017.
                                                </li>
                                                <li>
                                                    El premio será asignado a un ganador de la promoción más un acompañante mayor de edad de su elección.
                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            XXX gorras de la marca Budweiser.
                                        </li>
                                        <li>
                                            XXX cajas por 24 unidades de Budweiser.
                                        </li>

                                    </ul>
                                </li>
                                <li>
                                    <b>Mecanismos de participación y cronograma: </b>
                                    <ul>
                                        <li>
                                            El consumidor debe comprar cuatro (4) o seis (6) cervezas de Budweiser en botella NR de 343 ml en los puntos de venta participantes en la promoción, con lo cual recibirá una tarjeta que debe raspar para encontrar un código alfanumérico.
                                        </li>
                                        <li>
                                            El consumidor deberá ingresar a la página www.XXX.com y en el lugar correspondientes registrar sus datos personales y el código de la tarjeta. El usuario recibirá un correo electrónico con la confirmación de su participación.
                                        </li>
                                        <li>
                                            Luego de ingresar el código, el website le dirá de forma instantánea si ganó:
                                            <ul>
                                                <li>
                                                    Un (1) viaje doble para el segundo fin de semana de Tomorrowland 2017.
                                                </li>
                                                <li> Una gorra de Budweiser. XXX unidades disponibles. </li>
                                                <li> Una caja de 24 unidades de Budweiser. XXX unidades disponibles.</li>
                                            </ul>
                                        </li>
                                        <li>
                                            Ciudades participantes: Bogotá, Cali, Medellín, Barranquilla y Cartagena.
                                        </li>
                                        <li>
                                            A los ganadores de camisetas, gorras o cajas de cervezas el premio les será entregado por la marca en la dirección registrada en el website
                                        </li>

                                    </ul>
                                </li>
                                <li>
                                    <b> Quiénes pueden participar:</b>

                                    <br> Todas las personas naturales MAYORES DE EDAD que residan en Bogotá, Barranquilla, Cartagena, Cali y Medellín.
                                    <ul>

                                        <li>
                                            <b>Mecánica de la Actividad:</b> Para participar, los interesados deben:
                                            <ul>
                                                <li>
                                                    Comprar cuatro (4) unidades de Budweiser Botella NR de 343 ml en los puntos de venta identificados con la información de Tomorrowland Budweiser.
                                                </li>
                                                <li> Al comprar las cuatro (4) cervezas, el personal encargado de la promoción en el lugar le entregará al cliente una tarjeta raspe y gane que contendrá un código promocional.
                                                </li>
                                                <li>
                                                    Después de recibir la tarjeta con el código promocional, deberá rasparla e ingresar en la plataforma web de Budweiser para la actividad de Tomorrowland con el código que aparece en ésta.
                                                </li>
                                                <li> Una vez ingrese sus datos personales y luego el código, la plataforma informará si el consumidor es ganador de:
                                                    <ul>
                                                        <li>
                                                            Un paquete doble para
                                                        </li>
                                                        <li>
                                                            el segundo fin de semana de Tomorrowland 2017.
                                                        </li>
                                                        <li> Una gorra de Budweiser.
                                                        </li>
                                                        <li> Una camiseta de Budweiser.
                                                        </li>
                                                        <li>
                                                            Una caja de 24 unidades de Budweiser.
                                                        </li>
                                                    </ul>
                                                </li>
                                                <li> Todos los premios son intransferibles.

                                                </li>
                                            </ul>
                                        </li>
                                        <li>
                                            <b>Notificación de ganadores y entrega de premios:</b>
                                            <ul>
                                                <li>
                                                    La plataforma informará de manera automática al consumidor si es ganador de alguno de los premios antes descritos.
                                                </li>
                                                <li> De ganar el paquete doble para Tomorrowland, el ganador será notificado vía telefónica y vía email por representantes de la marca Budweiser en Colombia para coordinar la entrega del premio.
                                                </li>
                                                <li> De ganar gorra o caja de cerveza por 24 unidades, el premio será entregado única y exclusivamente en la dirección registrada por el participante en un plazo de 2 días.
                                                </li>

                                            </ul>
                                        </li>
                                        <li>
                                            <b>Condiciones y Restricciones Generales</b>
                                            <ul>
                                                <li>
                                                    Antes de participar en la actividad, el usuario admite que ha leído, entendido y aceptado los requisitos establecidos por el organizador de la actividad en estos Términos y Condiciones, los cuales estarán accesibles durante todo el tiempo de vigencia
                                                    de la actividad en <a href="http://www.bavaria.co/tomorrowlandbud" target="_blank">http://www.bavaria.co/tomorrowlandbud</a>. Si se llegase a presentar
                                                    algún cambio inesperado en las condiciones del concurso, este será comunicado responsablemente a través <a href="http://www.bavaria.co/tomorrowlandbudj" target="_blank">http://www.bavaria.co/tomorrowlandbudj</a>
                                                </li>
                                                <li> Podrán participar todas las personas naturales mayores de 18 años que residan en Bogotá, Barranquilla, Cali, Medellí y Cali y que estén presentes en estas ciudades durante las fechas en que tenga lugar la
                                                    actividad.
                                                </li>
                                                <li>
                                                    El ganador del paquete doble a Tomorrowland 2017 y su acompañante deberán contar con pasaporte vigente para hacer efectivo el premio. De no tenerlo, el ganador y/o su acompañante deberán hacerse cargo por cuenta propia de los trámites y costos asociados
                                                    a la expedición del pasaporte. De igual manera, el ganador debe estar en condiciones de viajar del 26 de julio de 2017 al 01 de agosto de 2017.
                                                </li>
                                                <li>
                                                    En caso de no cumplir con los prerequisitos del punto anterior, el premio será retornado al sorteo de modo que otro usuario pueda ganarlo con otro código.
                                                </li>
                                                <li>
                                                    El premio se entregará exclusivamente a los ganadores, previa verificación del cumplimiento de la mecánica del concurso (términos y condiciones) y de su mayoría de edad, para lo cual se solicitará una copia de su cédula de ciudadanía.
                                                </li>
                                                <li>
                                                    Para los efectos de esta actividad, se considera como motivo de descalificación que los participantes incumplan estos términos y condiciones. En caso de identificar fraude de un participante en la actividad, Bavaria S.A. podrá descalificarlo automáticamente
                                                    informando los motivos de su decisión mediante un correo electrónico.
                                                </li>
                                                <li>
                                                    Los premios no podrán ser canjeados por dinero en efectivo ni por ninguna otra especie.
                                                </li>
                                                <li>
                                                    El premio es una cortesía entregada por Bavaria S.A. y de ninguna manera podrá ser revendido o distribuido a terceros. De hacerlo, el ganador deberá responder legalmente a Bavaria S.A. por estos hechos.
                                                </li>
                                                <li>
                                                    Los ganadores conocen y entienden que Bavaria S.A. no es la organizadora del evento y que, en consecuencia, no se hacen responsables por su ejecución, la calidad del sonido, el cumplimiento de los artistas, o cualquier otro evento relacionado con la logística
                                                    del mismo.
                                                </li>
                                                <li>
                                                    Bavaria S.A. no se hace responsable de la integridad física, la salud, o la propiedad de los ganadores durante el evento o los desplazamientos asociados a este, ni de cualquier otro daño que estos sufran con ocasión de la participación en la actividad
                                                    o del disfrute del premio otorgado. Los ganadores entienden y aceptan lo anterior y, por tanto, exoneran a Bavaria S.A. de cualquier reclamación judicial o extrajudicial que pudiere derivarse de tales
                                                    eventos.
                                                </li>

                                            </ul>

                                            <li>
                                                <b>Protección de datos personales</b>
                                                <br> <br> Los datos personales y la información de los ganadores serán solicitados internamente en la plataforma con el único fin de hacer la entrega de los premios. Por tanto, estos datos no serán incorporados,
                                                recopilados o administrados en bases de datos, ni utilizados con fines comerciales, publicitarios o promocionales ni para cualquier otro fin diferente del aquí previsto, sino que serán eliminados al finalizar
                                                la entrega total de los premios.
                                            </li>
                                        </li>
                                </li>
                                </ul>

                                </p>

                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
    
    <!-- /.modal -->
    <div class="modal fade modal-type-winner" id="modal-viaje" data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                </div>
                <div class="modal-body">
                    <h3>AQUÍ COMIENZA TU VIAJE CON BUD, AL LUGAR MÁS LIBRE DEL PLANETA.</h3>
                    <img src="assets/tickets.png" alt="" class="img-responsive">
                    <h4>CON EL CÓDIGO <span id="codigo-viaje" class="codigo-ingr">xxxxxxxxx</span></h4>
                    <h1>¡GANASTE UN VIAJE DOBLE A TOMORROWLAND!</h1>
                    <P>RECIBE LA CONFIRMACIÓN VÍA MAIL</P>

                </div>
                <div class="modal-footer">
                    <a href="#modal-datos" data-toggle="modal" data-dismiss="modal">CONFIRMAR DATOS</a>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <footer class="container-fluid inside">
        <div class="container">

            <div class="copy">
                <div class="col-xs-12 col-sm-3">
                    <img src="assets/bud-tomorrowland.png" alt="" class="img-responsive">
                </div>
                <div class="col-xs-12 col-sm-6 advertiesement">
                    PROHÍBESE EL EXPENDIO DE BEBIDAS EMBRIAGANTES A MENORES DE EDAD. EL EXCESO DE ALCOHOL ES PERJUDICIAL PARA LA SALUD.
                </div>
                <div class="col-xs-12 col-sm-3">
                    <img src="assets/hablemosdealcohol.png" alt="" class="img-responsive">
                </div>
            </div>
        </div>
    </footer>
	<!-- bloque para recepcion de respuesta AJAX -->
	<div id="respuesta"></div>
    <!--  jQuery -->
    <script
        src="https://code.jquery.com/jquery-3.2.1.min.js"
        integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
        crossorigin="anonymous">   
    </script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <!-- Validador Parsley y lenguaje  -->
    <script src="js/parsley.min.js"></script>
    <script src="js/es.js"></script>
    <!-- Loading effect -->
    <script src="js/jquery.loading.min.js"></script>    
    <!-- Scripts generales -->
    <script>
    $(document).ready(function () {
      $('#frm-codigo').parsley().on('field:validated', function() {
        var ok = $('.parsley-error').length === 0;
        /* $('.bs-callout-info').toggleClass('hidden', !ok); */
        $('#alerta-campos').toggleClass('hidden', ok);
      });
      $("#frm-codigo").submit(function() {
        /*
		$("#btn-enviar").prop("disabled",true);
        $("#btn-enviar").attr("data-toggle", "tooltip");
        $("#btn-enviar").attr("title", "Sus datos fueron enviados!");
        $('[data-toggle="tooltip"]').tooltip();
        */
        $.ajax({type: 'POST',
          url: "premios.php",
          async: false,
          /* Mostrar splash de espera mientras llega respuesta del script php */
          beforeSend:
            function() {
              $.showLoading({name: 'jump-pulse',allowHide: false});
            },
          data: {
              'nombre'    : document.getElementById("nombre").value,
              'apellido'  : document.getElementById("apellido").value,
              'documento' : document.getElementById("documento").value,
			  'email'	  : document.getElementById("email").value,
              'telefono'  : document.getElementById("telefono").value,
              'ciudad'    : document.getElementById("ciudad").value,
              'direccion' : document.getElementById("direccion").value,
              'calle'     : document.getElementById("calle").value,
              'numero'    : document.getElementById("numero").value,
              'guion'     : document.getElementById("guion").value,
              'adicional' : document.getElementById("adicional").value,
              'pasaporte' : $('input[name=optradio]:checked').val(),
              'codigo'    : document.getElementById("keycode").value
              },
          /* Colocar respuesta del script php en el marco DIV indicado */
          success:
            function(result){
              $("#respuesta").html(result);
              $.hideLoading();
            },
		  error:
			function(result) {
				$.hideLoading();
				alert("error:\r\n"+result);
			}
        });        
        return false;
      });
	  
	 $("#frm-verifica").submit(function() {
        $.ajax({type: 'POST',
          url: "premios.php",
          async: false,
          /* Mostrar splash de espera mientras llega respuesta del script php */
          beforeSend:
            function() {
              $.showLoading({name: 'jump-pulse',allowHide: false});
            },
          data: {
              'nombre'    : document.getElementById("ver-nombre").value,
              'apellido'  : document.getElementById("ver-apellido").value,
              'documento' : document.getElementById("ver-documento").value,
			  'email'	  : document.getElementById("ver-email").value,
              'telefono'  : document.getElementById("ver-telefono").value,
              'ciudad'    : document.getElementById("ver-ciudad").value,
              'direccion' : document.getElementById("ver-dir1").value,
              'calle'     : document.getElementById("ver-dir2").value,
              'numero'    : document.getElementById("ver-dir3").value,
              'guion'     : document.getElementById("ver-dir4").value,
              'adicional' : document.getElementById("ver-dir5").value,
              'pasaporte' : $('input[name=ver-optradio]:checked').val(),
			  'id'		  : document.getElementById("id-usuario").value
              },
          /* Colocar respuesta del script php en el marco DIV indicado */
          success:
            function(result){
              $("#respuesta").html(result);
              $.hideLoading();
            },
		  error:
			function(result) {
				$.hideLoading();
				alert("error:\r\n"+result);
			}
        });        
        return false;
      });
	 
	  $("#no-ganaste, #btn-correcto").on("click", function() {
		$(location).attr('href', 'index.html');
	  });
    });
    </script>    
</body>

</html>