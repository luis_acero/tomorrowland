// Sass configuration
var gulp = require("gulp");
var sass = require('gulp-sass');
var gutil = require("gulp-util");
var concat = require("gulp-concat");
var mainBowerFiles = require("main-bower-files");
var uglify = require("gulp-uglify");
var streamqueue = require('streamqueue');
var nano = require('gulp-cssnano');
var less = require("gulp-less");
var minifyCss = require('gulp-minify-css');
var rename = require("gulp-rename");

var debug = false;
var outputRoot = "";


gulp.task("lib_js", function() {
    debug = debug || false;

    var plugins = mainBowerFiles("**/*.js");
    if (debug) gutil.log(plugins);

    var src = gulp.src(plugins);
    if (!debug) src = src.pipe(uglify());

    return src
        .pipe(concat("lib.js"))
        .pipe(gulp.dest(outputRoot + "dist/"));
});

gulp.task("lib_css", function() {
    debug = debug || false;
    return streamqueue({ objectMode: true },
            gulp.src(mainBowerFiles("**/*.less")).pipe(less()),
            gulp.src(mainBowerFiles("**/*.css")))
        .pipe(concat('lib.css')).pipe(nano()).pipe(gulp.dest(outputRoot + "dist/"))
});

gulp.task('appcss', function() {
    gulp.src('devcss/*.sass')
        .pipe(sass())
        .pipe(rename({ suffix: ".min" }))
        .pipe(minifyCss({ compatibility: 'ie8' }))
        .pipe(gulp.dest("css/"))
});

gulp.task("default", ["lib_js", "lib_css"], function() {
    gulp.watch('devcss/*.sass', ['appcss']);
});

gulp.task("debug", function() {
    debug = true;
    gutil.log(gutil.colors.green('RUNNING IN DEBUG MODE'));
    gulp.start('default');
})