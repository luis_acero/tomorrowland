<?php
$id         = isset($_POST['id'])?$_POST['id']:'';
$nombre     = isset($_POST['nombre'])?$_POST['nombre']:'';
$apellido   = isset($_POST['apellido'])?$_POST['apellido']:'';
$documento  = isset($_POST['documento'])?$_POST['documento']:'';
$email      = isset($_POST['email'])?$_POST['email']:'';
$telefono   = isset($_POST['telefono'])?$_POST['telefono']:'';
$ciudad     = isset($_POST['ciudad'])?$_POST['ciudad']:'';
$dir1       = isset($_POST['direccion'])?$_POST['direccion']:'';
$dir2       = isset($_POST['calle'])?$_POST['calle']:'';
$dir3       = isset($_POST['numero'])?$_POST['numero']:'';
$dir4       = isset($_POST['guion'])?$_POST['guion']:'';
$dir5       = isset($_POST['adicional'])?$_POST['adicional']:'';
$pasaporte  = isset($_POST['pasaporte'])?$_POST['pasaporte']:'';
$codigo     = isset($_POST['codigo'])?$_POST['codigo']:'';
$pasaporte  = ($pasaporte == 'si')?1:0;

if(!$nombre && !$email) exit;

include ('includes/conn.php');
/**** Si viene el campo id es porque solo se debe actualizar ****/
if($id) {
    $sql="UPDATE `participantes` SET
            `nombre` = '$nombre',
            `apellido` = '$apellido',
            `documento` = '$documento',
            `email` = '$email',
            `telefono` = '$telefono',
            `ciudad` = '$ciudad',
            `dir1` = '$dir1',
            `dir2` = '$dir2',
            `dir3` = '$dir3',
            `dir4` = '$dir4',
            `dir5` = '$dir5',
            `pasaporte` = $pasaporte
        WHERE `participantes`.`id` = $id ";
    if (!$result = $mysqli->query($sql)) {
        echo "Disculpe, el sitio web esta experimentando algunos problemas.";
    }
?>
    <script>
        $("#modal-datos").modal('hide');
        $('#modal-gracias').modal({backdrop: "static"});
    </script>    
<?php    
    exit;
}

/**** Verificar que el codigo exista en la base de datos y traer su estado para saber ****
 **** si ha sido utilizado                                                            ****/
$sql = "SELECT estado FROM unique_codes WHERE codigo='$codigo'";
if (!$result = $mysqli->query($sql)) {
    echo "Disculpe, el sitio web esta experimentando algunos problemas.";
    exit;
}

/**** Verificar si el codigo existe en la base de datos  ****/
if ($result->num_rows === 0) {
?>
    <script>
        $('#modal-error').modal({backdrop: "static"});
        $( "#keycode" ).focus();
    </script>
<?php
    exit;
} else {
    $fila = $result->fetch_assoc();
    $estado = $fila['estado'];
}

/**** Verificamos el el codigo no se haya utilizado antes ****/
if($estado > 0) {
?>
    <script>
        $('#modal-usado').modal({backdrop: "static"});
        $( "#keycode" ).focus();
    </script>
<?php
    exit;   
}
/**** Verificamos que la misma cedula no tenga 3 premios recibidos ***/
$sql = "SELECT COUNT(*) AS cantidad
        FROM participantes
        WHERE documento = '$documento'
            AND LOWER(premio) <> 'nada'
        ";
$ganados = 0;
if (!$result= $mysqli->query($sql)) {
    echo "Disculpe, el sitio web esta experimentando algunos problemas.";
    exit;
} else {
    $fila = $result->fetch_assoc();
    $ganados = $fila['cantidad'];
}    

// --- Buscamos los codigos sin utilizar
$sql = "SELECT count(*) AS cantidad FROM unique_codes WHERE estado = 0";
if (!$result= $mysqli->query($sql)) {
    echo "Disculpe, el sitio web esta experimentando algunos problemas.";
    exit;
} else {
    $fila = $result->fetch_assoc();
    $codigos = $fila['cantidad'];
}

// --- Buscamos premios disponibles
for($x = 1; $x <= 3; $x++) {
    $sql = "SELECT (cantidad - otorgados) AS disponible FROM premios WHERE id = $x";
    if (!$result= $mysqli->query($sql)) {
        echo "Disculpe, el sitio web esta experimentando algunos problemas.";
        exit;
    } else {
        $fila = $result->fetch_assoc();
        switch($x) {
        case 1: $prize1 = $fila['disponible']; break;
        case 2: $prize2 = $fila['disponible']; break;
        case 3: $prize3 = $fila['disponible']; break;
        }
    }
}

// --- Buscamos el número de registros para saber si hay premio
$sql = "SELECT count(*) AS cantidad FROM participantes";
if (!$result= $mysqli->query($sql)) {
    echo "Disculpe, el sitio web esta experimentando algunos problemas.";
    exit;
} else {
    $fila = $result->fetch_assoc();
    $intentos = $fila['cantidad'];
}

// --- obtenemos premio aleatoriamente, aqui esta la magia del concurso
$idpremio   = 0;
if((($intentos + 1) % $FRECUENCIA) === 0 && $ganados < 3) {
    $por_viaje  = 1;
    $por_gorra  = 0;
    $por_caja   = 0;
    $total_gc   = $price1 + $prize2 + $prize3;
    $azar = mt_rand(1,100);
    if($total_gc === 0) $idpremio = 1;
    else { 
        if($azar == $por_viaje) $azar = mt_rand(1,100);
        if($azar == $por_viaje) $idpremio = 1;
        elseif ($azar <= 100) {
            $por_gorra = intval(($prize2 * 100) / $total_gc);
            $por_caja  = intval(($prize3 * 100) / $total_gc);
            $max = ($por_gorra > $por_caja)?$por_gorra:$por_caja;
            if($max <= 60) $idpremio = mt_rand(2,3);
            else {
                $azar = mt_rand(1, $max);
                if($azar <= $por_gorra) $idpremio = 2;
                elseif($azar <= $por_caja) $idpremio = 3;
            }
        }
    }   
}
/***** este bloque obtiene ganadores completamente al azar
 bof
$azar = mt_rand(1, $codigos);
if($azar == $prize1) $idpremio = 1;
elseif ($azar <= $prize2 && $azar <= $prize3) $idpremio = mt_rand(2,3);
elseif ($azar <= $prize2) $idpremio = 2;
elseif ($azar <= $prize3) $idpremio = 3;
eof
*****/

switch($idpremio) {
    case 1: $txt_premio = 'Viaje doble a Tomorrowland'; break;
    case 2: $txt_premio = 'Gorra'; break;
    case 3: $txt_premio = 'Caja de Cerveza'; break;
    default: $txt_premio = 'Nada';
}

// --- Guardamos los datos del participante y el resultado de su participacion
$sql = "INSERT INTO `participantes` (`nombre`, `apellido`, `documento`, `email`, `telefono`, `ciudad`,
                                     `dir1`, `dir2`, `dir3`, `dir4`, `dir5`, `pasaporte`, `codigo`, `premio`)
        VALUES('$nombre', '$apellido', '$documento', '$email', '$telefono', '$ciudad',
               '$dir1', '$dir2', '$dir3', '$dir4', '$dir5', $pasaporte, '$codigo', '$txt_premio')";
if (!$result= $mysqli->query($sql)) {
        echo "Disculpe, el sitio web esta experimentando algunos problemas.";
        exit;
}
$id = $mysqli->insert_id;

// --- Actualizamos status del codigo utilizado
$sql = "UPDATE unique_codes SET estado=1 WHERE codigo = '$codigo'";
if (!$result= $mysqli->query($sql)) {
        // Oh no! The query failed. 
        echo "Disculpe, el sitio web esta experimentando algunos problemas.";
        exit;
}

// --- Actualizamos los premios disponibles para mantener la probabilidad
if(intval($idpremio) > 0) {
    $sql = "UPDATE premios SET otorgados = otorgados + 1 WHERE id = $idpremio";
    if (!$result= $mysqli->query($sql)) {
            echo "Disculpe, el sitio web esta experimentando algunos problemas.";
            exit;
    }
    // --- Rellenamos el formulario de confirmacion
?>
<script>
    $("#ver-nombre").val("<?php print $nombre ?>");
    $("#ver-apellido").val("<?php print $apellido ?>");
    $("#ver-documento").val("<?php print $documento ?>");
    $("#ver-email").val("<?php print $email ?>");
    $("#ver-telefono").val("<?php print $telefono ?>");
    $("#ver-ciudad").val("<?php print $ciudad ?>");
    $("#ver-dir1").val("<?php print $dir1 ?>");
    $("#ver-dir2").val("<?php print $dir2 ?>");
    $("#ver-dir3").val("<?php print $dir3 ?>");
    $("#ver-dir4").val("<?php print $dir4 ?>");
    $("#ver-dir5").val("<?php print $dir5 ?>");
    $('#<?php print ($pasaporte == 1)?"s":"n"; ?>-option-ver').prop('checked', true);
    $("[name='ver-optradio']").val("<?php print ($pasaporte == 1)?'si':'no'; ?>");
    $("#id-usuario").remove();
    $('<input>', {
        type : 'hidden',
        id   : 'id-usuario',
        name : 'id-usuario',
        value: '<?php print $id ?>'
      }).appendTo('#frm-verifica');
    $("#codigo-viaje").html("<?php print $codigo ?>");
    $("#codigo-gorra").html("<?php print $codigo ?>");
    $("#codigo-gorra2").html("<?php print $codigo ?>");
    $("#codigo-caja").html("<?php print $codigo ?>");
</script>
<?php

}
// --- Si hay premio enviar un email al usuario
if($idpremio > 0) {
    $headers = "MIME-Version: 1.0" . "\r\n";
    $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
    $headers .= 'From: Web Master <webmaster@a2hosted.com>' . "\r\n" . "\r\n";
    //-- Mensaje personalizado segun tipo de premio
    //-- Caja de cerveza
    switch($idpremio) {
    case 2:
        $mensaje = "<p>¡Hola $nombre! Gracias por participar, </p>";
        $mensaje .="<p>una gorra de Budweiser para que lleves el estilo High Energy en tus propios términos.</p>";
        $mensaje .="<p>¡Muy pronto la recibirás en la dirección que registraste! Si más adelante quieres tener información de este envío, ";
        $mensaje .= "envíanos un inbox a nuestro fanpage para atender tu caso: https://www.facebook.com/CervezaBudweiserColombia/ ";
        $mensaje .= "<p>Te invitamos a seguir participando por el gran premio del viaje doble a Tomorrowland. ";
        $mensaje .= "Puedes comprar otra promo en el mismo punto de venta u otro que encontrarás en la misma página donde ingresaste el código."; 
        $mensaje .= "¡Y tienes otra forma de ganar! Entérate siguiendo las últimas publicaciones de nuestro fanpage. </p>";
        $mensaje .= "#TomorrowlandConBud";
        break;        
    case 3:
        $mensaje = "<p>¡Hola $nombre! Gracias por participar, </p>";
        $mensaje .="<p>Ganaste una caja de cerveza (x24) para que celebres en tus propios términos con la más alta energía.</p>";
        $mensaje .="<p>¡Muy pronto la recibirás en la dirección que registraste! Si más adelante quieres tener información de este envío, ";
        $mensaje .= "envíanos un inbox a nuestro fanpage para atender tu caso: https://www.facebook.com/CervezaBudweiserColombia/ ";
        $mensaje .= "<p>Te invitamos a seguir participando por el gran premio del viaje doble a Tomorrowland. ";
        $mensaje .= "Puedes comprar otra promo en el mismo punto de venta u otro que encontrarás en la misma página donde ingresaste el código."; 
        $mensaje .= "¡Y tienes otra forma de ganar! Entérate siguiendo las últimas publicaciones de nuestro fanpage. </p>";
        $mensaje .= "#TomorrowlandConBud";
        break;
    default:
        $mensaje = "<p>Hola $nombre!,</p><p> Eres el GRAN GANADOR DEL VIAJE DOBLE A TOMORROWLAND.</p>";
        $mensaje .="<p>Guarda este correo y escríbenos un inbox a nuestro fanpage con tu código: <a href='https://www.facebook.com/CervezaBudweiserColombia/'>https://www.facebook.com/CervezaBudweiserColombia/</a>
<br>
¡CONSERVA EL RASPE Y GANE! Tendrás que presentarlo para reclamar el premio.
<br>
#TomorrowlandConBud</p>"; 
    }
    mail($email,"Felicidades has ganado con Budweiser!", $mensaje, $headers);
    if($idpremio == 1) {
        $mensaje = "Hola Administrador, hay un ganador para el premio mayor: <br />
            <strong>Nombre:</strong> $nombre<br />
            <strong>Apellido:</strong> $apellido<br />
            <strong>Premio:</strong> Viaje a Tomorrowland<br />
            <strong>Código:</strong> $codigo<br />
            <strong>Dirección:</strong> $ciudad $dir1 $dir2 $dir3 $dir4 $dir5";
        mail("camilo.fajardo@vice.com, fernando.perilla@dynamikcollective.com, miguel.pombo@vice.com, francisco.quinones@vice.com","ALERTA DE PREMIO MAYOR - Tomorrowland con Bubweiser", $mensaje, $headers);
    }
}

// --- Una vez actualizada la base de datos se personaliza el mensaje a mostrar al usuario
switch($idpremio) {
    case 0: $modal = 'modal-nada'; break;
    case 1: $modal = 'modal-viaje'; break;
    case 2: $modal = 'modal-gorra'; break;
    case 3: $modal = 'modal-pack';
}
?>
    <script>
        $('#<?php print $modal ?>').modal({backdrop: "static"});
    </script>
<?php
/*
$result->free();
$mysqli->close();
*/
?>